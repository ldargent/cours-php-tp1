<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Exercice 10</title>
</head>
<body>

<?php
$mesNotes = array(5, 10, 2, 8);

// Calcul sans fonction
$somme = 0;
foreach ($mesNotes as $note) {
    $somme += $note;
}

echo "<p>Somme du tableau (sans fonction) : ".$somme."</p>";



// Calcul avec fonction
function somme($nombres) {
    $somme = 0;
    foreach ($nombres as $nombre) {
        $somme += $nombre;
    }

    return $somme;
}

echo "<p>Somme du tableau (avec fonction) : ".somme($mesNotes)."</p>";



// Calcul avec fonction et typage
function sommeAvecTypage(array $nombres): int {
    $somme = 0;
    foreach ($nombres as $nombre) {
        $somme += $nombre;
    }

    return $somme;
}

echo "<p>Somme du tableau (avec fonction et typage) : ".sommeAvecTypage($mesNotes)."</p>";

?>
</body>
</html>