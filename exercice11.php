<?php
$etudiants = array();
$etudiants[] = array(
    'Eric', 22,
    'notes' => array(10, 15, 8, 16, 15)
);
$etudiants[] = array(
    'Sylvie', 25,
    'notes' => array(5, 6, 8, 10, 8)
);
$etudiants[] = array(
    'Luc', 21,
    'notes' => array(15, 15, 18, 14, 15)
);
$etudiants[] = array(
    'Simon', 22,
    'notes' => array(8, 6, 20, 4, 13)
);



// Calcul de la moyenne
function moyenne($notes) {
    $somme = 0;
    foreach ($notes as $note) {
        $somme += $note;
    }

    return $somme / count($notes);
}

// Calcul du nombre d'étudiant ayant la moyenne
function nbEtudiantAvecMoyenne($etudiants) {

    $nbEtudiantsMoy = 0;
    foreach ($etudiants as $etudiant) {
        if (moyenne($etudiant['notes']) >= 10) {
            $nbEtudiantsMoy++;
        }
    }

    return $nbEtudiantsMoy;
}

// Calcul du nombre d'étudiant ayant obtenu une certaine note
function nbEtudiantAvecNote($etudiants, $note) {

    $nbEtudiantsNote = 0;
    foreach ($etudiants as $etudiant) {
        if (in_array($note, $etudiant['notes'])) {
            $nbEtudiantsNote++;
        }
    }

    return $nbEtudiantsNote;
}

// Recherche la meilleur note de tous les étudiants
function meilleurNote($etudiants) {

    $meilleurNote = array('nom' => '', 'note' => 0);

    foreach ($etudiants as $etudiant) {
        if (max($etudiant['notes']) > $meilleurNote['note']) {
            $meilleurNote['nom'] = $etudiant[0];
            $meilleurNote['note'] = max($etudiant['notes']);
        }
    }

    return $meilleurNote;
}

// Affiche le tableau des étudiants
function afficheTableauEtudiant($etudiants) {

    // boucle sur l'ensemble des étudiants
    foreach ($etudiants as $etudiant) {

        $moyenne = moyenne($etudiant['notes']);

        // calcul la couleur de la ligne + stat
        $couleurLigne = 'green';
        if ($moyenne < 10) {
            $couleurLigne = 'red';
        }

        echo "<tr style='color: ".$couleurLigne."'>";
        echo "<td>".$etudiant[0]."</td>";
        echo "<td>".$etudiant[1]."</td>";
        echo "<td>".$moyenne."</td>";
        echo "</tr>";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Exercice 11</title>
</head>
<body>

<!-- Boucle Foreach -->
<table>
    <tr>
        <td>Nom</td>
        <td>Age</td>
        <td>Moyenne</td>
    </tr>
    <?php

    $meilleurNote = meilleurNote($etudiants);
    afficheTableauEtudiant($etudiants);

    ?>
</table>

Nombre d'étudiant avec plus de 10 de moyenne : <?php echo nbEtudiantAvecMoyenne($etudiants); ?>
<br/>
Nombre d'étudiant ayant obtenu au moins un 15 : <?php echo nbEtudiantAvecNote($etudiants, 15); ?>
<br/>
Meilleur note de <?php echo $meilleurNote['note']; ?> obtenu par <?php echo $meilleurNote['nom']; ?>

</body>
</html>