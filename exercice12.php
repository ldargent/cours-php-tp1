<?php

// Récupère le nom
if (isset($_POST['nom']) && $_POST['nom'] !== "") {
    $nom = htmlspecialchars($_POST['nom']);
}

// Récupère l'age
if (isset($_POST['age']) && $_POST['age'] !== "") {
    $age = (int) htmlspecialchars($_POST['age']);
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Exercice 12</title>
</head>
<body>

<?php
// Affiche le récapitulatif du formulaire
if (isset($nom) && isset($age)) {
    echo "<p>Nom: $nom</p>";
    echo "<p>Age: $age</p>";
}

?>

<form method="post" action="exercice12.php">
    Nom : <input type="text" name="nom">
    Age : <input type="text" name="age">
    <input type="submit">
</form>

</body>
</html>