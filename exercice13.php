<?php

$nom = null;
$prenom = null;
$genre = null;
$type = null;
$message = null;
$messageOriginal = "";

$genreId = null;
$typeId = null;

$genreValeurs = array('M.', 'Mme', 'Mlle');
$typeValeurs = array('Enfant', 'Adulte');

$erreurs = array();

// Récupère le nom
if (isset($_POST['nom']) && $_POST['nom'] !== "") {
    $nom = htmlspecialchars($_POST['nom']);
}

// Récupère le prénom
if (isset($_POST['prenom']) && $_POST['prenom'] !== "") {
    $prenom = htmlspecialchars($_POST['prenom']);
}

// Récupère le genre
if (isset($_POST['genre']) && $_POST['genre'] !== "") {
    $genreId = (int) $_POST['genre'];
    $genre = $genreValeurs[$_POST['genre']];
}

// Récupère le type
if (isset($_POST['type']) && $_POST['type'] !== "") {
    $typeId = (int) $_POST['type'];
    $type = $typeValeurs[$_POST['type']];
}

// Récupère le message
if (isset($_POST['message']) && $_POST['message'] !== "") {
    $messageOriginal = htmlspecialchars($_POST['message']);
    $message = nl2br(htmlspecialchars($_POST['message']));
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Exercice 13</title>
</head>
<body>

<h1>Formulaire de contact</h1>

<?php

// Gestion des messages d'erreur
if (!empty($_POST)) {

    if (is_null($nom)) {
        $erreurs[] = "Veuillez saisir un nom";
    }

    if (is_null($type)) {
        $erreurs[] = "Veuillez sélectionner un type";
    }

    if (count($erreurs) > 0) {

        echo "<div style='color: red'>";
        echo "<p>Erreurs de saisie :</p>";

        foreach ($erreurs as $erreur) {
            echo "<p>".$erreur."</p>";
        }
        echo "</div>";
    }
}

?>

<form method="post" action="exercice13.php">
    <p>
        Nom * : <input type="text" name="nom" value="<?php echo $nom; ?>">
    </p>
    <p>
        Prénmom : <input type="text" name="prenom" value="<?php echo $prenom; ?>">
    </p>
    <p>
        Genre :
        <select name="genre">
            <?php
            foreach ($genreValeurs as $key => $genreValeur) {
                $selected = (!is_null($genre) && $genreId === $key) ? 'selected=selected' : '';
                echo '<option value="'.$key.'" '.$selected.'>'.$genreValeur.'</option>';
            }
            ?>
        </select>
    </p>
    <p>
        Type * :
        <?php
        foreach ($typeValeurs as $key => $typeValeur) {
            $selected = (!is_null($genre) && $typeId === $key) ? 'checked=checked' : '';
            echo '<input type="radio" name="type" value="'.$key.'" '.$selected.'>'.$typeValeur;
        }
        ?>
    </p>
    <p>
        Message :
        <textarea name="message"><?php echo $messageOriginal; ?></textarea>
    </p>
    <input type="submit" name="envoi">
    <p>* Champs obligatoires</p>
</form>

<?php
// Affiche le récapitulatif du formulaire
if (!empty($_POST) && count($erreurs) == 0) {
    echo "<h1>Récapitulatif de vos informations</h1>";
    echo "<p>Nom: $nom</p>";
    echo "<p>Prénom: $prenom</p>";
    echo "<p>Genre: ".$genre."</p>";
    echo "<p>Type: ".$type."</p>";
    echo "<p>Message: ".$message."</p>";
}

?>

</body>
</html>