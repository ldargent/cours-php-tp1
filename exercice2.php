<?php
$nom = "Dupont";
$prenom = "Louis";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Exercice 2</title>
</head>
<body>
<?php

// 1ère méthode
echo $nom;
echo " ";
echo $prenom;
echo "<br/>";

// 2ème méthode
echo "$nom $prenom";
echo "<br/>";

// 3ème méthode
echo $nom." ".$prenom;

?>
</body>
</html>