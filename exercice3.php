<?php
define("TVA", 0.206);
$prixTTC = 150;
$nombre = 10;
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Exercice 3</title>
</head>
<body>
<h1>Calcul sur les variables</h1>

<?php

echo "Prix TTC des $nombre articles :" . $prixTTC * $nombre;
echo "<br/>";
echo "Prix HT des $nombre articles :" . ($prixTTC * (1 - TVA)) * $nombre;
echo "<br/>";

// Type des variables
echo 'TVA -> '.gettype(TVA); echo "<br/>";
echo '$prixTTC -> '.gettype($prixTTC); echo "<br/>";
echo '$nombre -> '.gettype($nombre);

?>
</body>
</html>