<?php
$prixTable = 150;
$prixArmoire = 50;
$nombre = 10;
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Exercice 4</title>
</head>
<body>
<h1>Calcul sur les variables</h1>

<?php

echo "Prix des $nombre tables :" . $prixTable * $nombre;
echo "<br/>";

if ($prixTable > $prixArmoire) {
    echo "La table est plus cher que l'armoire";
} else {
    echo "L'armoire est plus cher que la table";
}

?>
</body>
</html>