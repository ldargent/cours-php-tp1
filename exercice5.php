<?php
$nombre = 10;
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Exercice 5</title>
</head>
<body>

<?php

$somme = 0;

// Boucle While
$i = 1;
while ($i < $nombre) {
    $somme += $i;
    $i++;
}

echo "Somme des entiers avec une boucle While : " . $somme;
echo "<br/>";

// ----------------------------------------------------------------

$somme = 0;

// Boucle For
for ($i = 1; $i < $nombre; $i++) {
    $somme += $i;
}

echo "Somme des entiers avec une boucle For : " . $somme;

?>
</body>
</html>