<?php
$etudiants = array();
$etudiants[] = array('Eric', 22, 12);
$etudiants[] = array('Sylvie', 25, 8);
$etudiants[] = array('Luc', 21, 15);
$etudiants[] = array('Simon', 22, 18);
$etudiants[] = array('Léa', 22, 17);
$etudiants[] = array('John', 27, 6);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Exercice 6</title>
</head>
<body>

<!-- Boucle For -->
<table>
    <tr>
        <td>Nom</td>
        <td>Age</td>
        <td>Moyenne</td>
    </tr>
    <?php
    // boucle sur l'ensemble des étudiants
    for ($i = 0; $i < count($etudiants); $i++) {
        echo "<tr>";
        echo "<td>".$etudiants[$i][0]."</td>";
        echo "<td>".$etudiants[$i][1]."</td>";
        echo "<td>".$etudiants[$i][2]."</td>";
        echo "</tr>";
    }
    ?>
</table>

<!-- Boucle Foreach -->
<table>
    <tr>
        <td>Nom</td>
        <td>Age</td>
        <td>Moyenne</td>
    </tr>
    <?php
    // boucle sur l'ensemble des étudiants
    foreach ($etudiants as $etudiant) {
        echo "<tr>";
        echo "<td>".$etudiant[0]."</td>";
        echo "<td>".$etudiant[1]."</td>";
        echo "<td>".$etudiant[2]."</td>";
        echo "</tr>";
    }
    ?>
</table>
</body>
</html>