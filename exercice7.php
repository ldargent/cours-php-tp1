<?php
$etudiants = array();
$etudiants[] = array(
    'Eric', 22,
    'notes' => array(10, 15, 8, 16, 15)
);
$etudiants[] = array(
    'Sylvie', 25,
    'notes' => array(5, 6, 8, 10, 8)
);
$etudiants[] = array(
    'Luc', 21,
    'notes' => array(15, 15, 18, 14, 15)
);
$etudiants[] = array(
    'Simon', 22,
    'notes' => array(8, 6, 20, 4, 13)
);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Exercice 7</title>
</head>
<body>

<!-- Boucle Foreach -->
<table>
    <tr>
        <td>Nom</td>
        <td>Age</td>
        <td>Moyenne</td>
    </tr>
    <?php

    $meilleurNote = array('nom' => '', 'note' => 0);
    $nbEtudiantMoySup = 0;
    $nbEtudiantNote = 0;

    // boucle sur l'ensemble des étudiants
    foreach ($etudiants as $etudiant) {

        // calcul de la moyenne
        $notesTotal = 0;
        foreach ($etudiant['notes'] as $note) {
            $notesTotal += $note;
            // Détetection de la meilleur note obtenu
            if ($note > $meilleurNote['note']) {
                $meilleurNote['nom'] = $etudiant[0];
                $meilleurNote['note'] = $note;
            }
        }
        $moyenne = $notesTotal / count($etudiant['notes']);

        // recherche les étudiants ayant obtenu au moins une note de 15
        if (in_array(15, $etudiant['notes'])) {
            $nbEtudiantNote++;
        }

        // calcul la couleur de la ligne + stat
        if ($moyenne < 10) {
            $couleurLigne = 'red';
        } else {
            $couleurLigne = 'green';
            $nbEtudiantMoySup++;
        }

        echo "<tr style='color: ".$couleurLigne."'>";
        echo "<td>".$etudiant[0]."</td>";
        echo "<td>".$etudiant[1]."</td>";
        echo "<td>".$moyenne."</td>";
        echo "</tr>";
    }
    ?>
</table>

Nombre d'étudiant avec plus de 10 de moyenne : <?php echo $nbEtudiantMoySup; ?>
<br/>
Nombre d'étudiant ayant obtenu au moins un 15 : <?php echo $nbEtudiantNote; ?>
<br/>
Meilleur note de <?php echo $meilleurNote['note']; ?> obtenu par <?php echo $meilleurNote['nom']; ?>

</body>
</html>