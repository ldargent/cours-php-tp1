<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Exercice 9</title>
</head>
<body>

<?php
$texte = <<< EOT
King Tubby a expérimenté le travail en studio avec des bandes magnétiques
(à l'époque le seul moyen d'enregistrement !) et utilisé le décalage physique (et donc temporel à l'écoute...)
de plusieurs magnétophones afin de produire un effet d'écho sur les guitares, 
voix et batteries des morceaux qu'il mixait";
EOT;

// Statistique du texte
echo "<p>".$texte."</p>";
echo "<p>Nombre de caractères : ".strlen($texte)."</p>";
echo "<p>Nombre de mots : ".str_word_count($texte)."</p>";
echo "<p>Nombre d'occurence de la chaîne 'et' : ".substr_count($texte, 'et')."</p>";

// Remplacement de l'occurence 'le' par 'la' en gras
$texte2 = str_replace('le', '<b>la</b>', $texte);
echo "<p>".$texte2."</p>";

// Affichage de la première moitié du texte
echo "<p>".substr($texte, 0, strlen($texte) / 2)."</p>";

?>
</body>
</html>